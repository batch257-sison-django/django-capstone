from django.urls import path
from . import views

app_name = 'todolist'
urlpatterns = [
    path('', views.index, name = "index"),

    # tasks
    path('task/add-task/', views.add_task, name="add_task"),
    path('task/<int:todoitem_id>/', views.todoitem, name = "viewtodoitem"),
    path('task/<int:todoitem_id>/edit', views.update_task, name="update_task"),
    path('task/<int:todoitem_id>/delete', views.delete_task, name="delete_task"),

    # events section
    path('event/add-event/', views.add_event, name="add_event"),
    path('event/<int:eventitem_id>/', views.eventitem, name = "vieweventitem"),
    path('event/<int:eventitem_id>/edit', views.update_event, name="update_event"),
    path('event/<int:eventitem_id>/delete', views.delete_event, name="delete_event"),


    # accounts section 
    path('change-password/', views.changePassword, name = "change_password"),
    path('login/', views.login_view, name = "login"),
    path('logout/', views.logout_view, name = "logout"),
    path('register/', views.register, name = "register")
]